# GAME API

If we want people to access our game via multiple platforms, we need to create a
server for it. The good news are: the game engine is a library and we can use it
here!

So, let's update the `package.json` file to include your game engine. You might
notice that some libraries are already included with this project, and there's a
script called `docs` there too. Try it out running `npm run docs`, and check the
new `docs` folder.

Magic? No. Your documentation was generated from the `api.yml` file, which is a
specification of how your API will behave.

Describing the API behavior might give us a few benefits, such as mocking it so
we can develop frontend and backend at the same time.

# 💡 EXERCISE 1 💡

Take a look at [PRISM](https://stoplight.io/open-source/prism), a tool you can
use to mock your API. Learn how to do it, and include some examples on the
`api.yml` file. You can look for examples on
[this link](https://stoplight.io/p/docs/gh/stoplightio/prism/docs/guides/mock-responses.md).
