const new_game = (request, response) => {
  response.json({
    status: "RUNNING",
    lives: 4,
    word: "banana",
    guesses: [],
    display_word: "_ _ _ _ _ _ "
  });
};

module.exports = { new_game };
