const apiGenerator = require("./api_generator");

apiGenerator()
  .then(app => app.listen(3000))
  .then(() => {
    console.log("Server started");
  })
  .catch(err => {
    console.error("caught error", err);
  });
