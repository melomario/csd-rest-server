const express = require("express");
const SwaggerParser = require("swagger-parser");
const { connector } = require("swagger-routes-express");
const api = require("./api");

const apiGenerator = async () => {
  const parser = new SwaggerParser();
  const apiDescription = await parser.validate("api.yml");
  const connect = connector(api, apiDescription);
  const app = express();
  connect(app);
  return app;
};
module.exports = apiGenerator;
